clamsmtp (1.10-15) unstable; urgency=medium

  Please note that the service file does not check clamsmtpd.conf for a
  different pidfile location. If you moved the file to a different directory,
  please adjust the service accordingly.

  The default file has not really been needed since version 0.8.0 when
  /etc/clamsmtpd.conf was introduced. It is time to finally remove it from the
  package.

 -- Michael Meskes <meskes@debian.org>  Mon, 19 Feb 2018 13:08:49 +0100

clamsmtp (1.3-0) unstable; urgency=low

  In the interest of least privilege security, a new system user and
  group was created for clamsmtp.  The clamav-daemon user, clamav, was
  added to the clamsmtp group, allowing the daemon to view the files
  in the quarantine directory.

 -- Chad Walstrom <chewie@debian.org>  Sat, 26 Feb 2005 15:38:12 -0600

clamsmtp (1.2-2) unstable; urgency=low

  The semantics for fixing directory permissions has been changed, and now only
  applies to fixing older version of this package.  The "fix" has moved out of
  the init.d script to the postinst script.  New debconf translations have been
  added.

 -- Chad Walstrom <chewie@debian.org>  Thu, 13 Jan 2005 10:28:55 -0600

clamsmtp (1.1-2) unstable; urgency=low

  debconf config was added this round to ask some important questions.  Some
  sanity checking was added to the init script to make sure directory
  permissions were configured correctly.  /var/run/clamsmtp and
  /var/spool/clamsmtp are now created in postinst and purged in postrm.

 -- Chad Walstrom <chewie@debian.org>  Wed, 24 Nov 2004 18:17:04 -0600

clamsmtp (1.0-1) unstable; urgency=low

  Some versions of the Linux 2.4.x kernel caused 0.9 to crash.  Version 0.9.5
  fixed this bug.  Also, as of 0.9.5, you can specify the PID file in the
  configuration file.

 -- Chad Walstrom <chewie@debian.org>  Wed, 27 Oct 2004 10:21:07 -0500

clamsmtp (0.9-1) unstable; urgency=low

  This new upstream version allows administrators to specify an action script
  to run when a virus is found. (Be careful.)  The daemon can now switch users
  upon startup, so the init.d script was updated.

 -- Chad Walstrom <chewie@debian.org>  Wed, 22 Sep 2004 15:41:24 -0500

clamsmtp (0.8-2) unstable; urgency=low

  Fixed the permissions to the /var/spool/clamsmtp directory.

 -- Chad Walstrom <chewie@debian.org>  Wed,  8 Sep 2004 22:32:20 -0500

clamsmtp (0.8-1) unstable; urgency=low

  The addition of the configuration file /etc/clamsmtpd.conf makes things much
  cleaner for the /etc/defaults/clamsmtp file. 

  The /var/lib/clamsmtp directory was moved to /var/spool to confirm to FHS.
  clamsmtp uses /tmp for its spool files by default, but this makes them
  susceptible to resource starvation should the /tmp partition fill up.
    
  I am not going to try to graceful move the content of the /var/lib/clamsmtp
  directory, given that the last binary package was 'experimental'.

 -- Chad Walstrom <chewie@debian.org>  Tue,  7 Sep 2004 22:28:40 -0500

